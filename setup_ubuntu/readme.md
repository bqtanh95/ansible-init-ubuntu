# Initial Server Setup on Ubuntu 20.04

This playbook will execute a initial server setup for Ubuntu 20.04 systems, as explained in the guide on
[Initial Server Setup Guide for Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-use-ansible-to-automate-initial-server-setup-on-ubuntu-20-04).
A number of containers will be created with the options specified in the `vars/default.yml` variable file.

## Running this Playbook

Quick Steps:

### 1. Obtain the playbook
```shell
cd ansible
```

### 2. Customize Options

```shell
nano vars/default.yml
```

### 3. Run the Playbook

```command
cd setup_ubuntu
ansible-playbook -l [target] -i [inventory file] -u [remote user] playbook.yml

OR

make run_setup_ubuntu
```

## Tested
- Ubuntu 18.04
- Ubuntu 20.04
- Ubuntu 22.04