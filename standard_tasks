# Standard tasks to standardize all remote servers
# Note that these 2 var files are needed:
# vars_files:
#   - vars/standard.yml
#   - vars/secret/ssh_key.yml
---
- name: Ensure PAM is enabled for SSH
  lineinfile: "dest=/etc/ssh/sshd_config state=present regexp='^UsePAM' line='UsePAM yes'"
  become: yes
- name: Install nexionpost script
  template: src=templates/nexionpost dest=/usr/bin/nexionpost mode="a+x"
  become: yes
- name: Install login-notify script
  template: src=templates/login-notify dest=/usr/bin/login-notify mode="a+x"
  become: yes
- name: Add login-notify to PAM config
  lineinfile: "dest=/etc/pam.d/sshd state=present line='session    optional     pam_exec.so seteuid /usr/bin/login-notify'"
  become: yes
- name: Set timezone variables
  copy: content='{{ time_zone }}'
    dest=/etc/timezone
    owner=root
    group=root
    mode=0644
    backup=yes
  become: yes
- name: update timezone
  command: dpkg-reconfigure --frontend noninteractive tzdata
  become: yes
- name: install packages
  apt:
    name:
      - fail2ban
      - unattended-upgrades
      - tmpreaper
      - ntp
    state: present
  become: yes
- name: Deploy tmpreaper config
  template: src=templates/etc/tmpreaper.conf dest=/etc/tmpreaper.conf
  become: yes
- name: Deploy unattended upgrade file
  template: src=templates/apt/apt.conf.d/50unattended-upgrades dest=/etc/apt/apt.conf.d/50unattended-upgrades mode=0644
  become: yes
- name: Deploy unattended schedule file
  template: src=templates/apt/apt.conf.d/10periodic dest=/etc/apt/apt.conf.d/10periodic mode=0644
  become: yes
- name: create group admin
  group: name=admin state=present
  become: yes
- name: create group geekup
  group: name=geekup state=present
  become: yes
- name: create user geekup
  user: name={{ app_user }} groups="admin,geekup" shell=/bin/bash password="$6$rounds=100000$g1/SqhRF5v7TsGdd$7/dcdP9qhlZaC0TPZ5fWNDkILAjFmeR38bFOm7sAHhoGpDnBs3CB/mBbENwJlqrrLUS9VPHQ1GMfcffa0vQFh."
  become: yes
- name: ensure geekup .ssh directory exists
  file: path=/home/{{ app_user }}/.ssh state=directory
  become: yes
  become_user: "{{ app_user }}"
- name: add private key for geekup user
  copy: content="{{ ssh_key }}" dest=/home/{{ app_user }}/.ssh/geekup mode=400
  become: yes
  become_user: "{{ app_user }}"
- name: add public key for geekup user
  authorized_key: user={{ app_user }} key="{{ item }}"
  with_file:
    - geekup.pub
  become: yes
  become_user: "{{ app_user }}"
- name: add devops key for geekup user
  authorized_key: user={{ app_user }} key="{{ item }}"
  with_file:
    - devops.pub
  become: yes
  become_user: "{{ app_user }}"
- name: make geekup user able to sudo without password
  lineinfile: "dest=/etc/sudoers state=present regexp='^%admin' line='%admin ALL=(ALL) NOPASSWD: ALL'"
  become: yes
- name: add gitlab to geekup user's known hosts
  known_hosts: path='/home/{{ app_user }}/.ssh/known_hosts' host='gitlab.geekup.io' key="{{ lookup('file', 'files/gitlab.pub') }}"
  become: yes
  become_user: "{{ app_user }}"
- include: tasks/enable_swap.yaml
