connect_all:
	ansible all -m ping

test:
	ansible test -m command -a 'df -h'

run_setup_ubuntu:
	cd setup_ubuntu && ansible-playbook -l ubuntu -i ../hosts playbook.yml